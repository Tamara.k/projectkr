package neww.example.KR2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class APIController {



        @Autowired
        private JdbcTemplate jdbcTemplate;

        @PostMapping("/radar")
        public void handleNewSelectedItem(@RequestBody Kasutaja kasutaja){
            System.out.println("handleNewSelectedItem");
            System.out.println("Kasutaja  vastus1: " + kasutaja.getVastus1());
            String sqlKask = "INSERT INTO kasutajad (vastus1, vastus2, vastus3, vastus4, vastus5, vastus6, vastus7, vastus8, vastus9) VALUES ('"
                    + kasutaja.getVastus1() + "', '" + kasutaja.getVastus2() + "', '" + kasutaja.getVastus3() + "', '" + kasutaja.getVastus4() + "', '" + kasutaja.getVastus5()
                    + "', '" + kasutaja.getVastus6() + "', '" + kasutaja.getVastus7() + "', '" + kasutaja.getVastus8() + "', '" + kasutaja.getVastus9() + "');";
            jdbcTemplate.execute(sqlKask);
            System.out.println("Sisestamine õnnestus!");
        }

        @GetMapping("/radar")
        public ArrayList<Kasutaja> printListItems(){
            System.out.println("printListItems käivitus");
            ArrayList<Kasutaja> vastused = new ArrayList<>();
            vastused = (ArrayList)jdbcTemplate.queryForList("SELECT * FROM kasutajad ORDER BY id DESC LIMIT 1");
            return vastused;
        }
    }

